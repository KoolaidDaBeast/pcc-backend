const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const cookieParser = require('cookie-parser');
const app = express();
const port = process.env.PORT || 5000;
const BookingHelper = require('./helper/BookingHelper');
const AccountHelper = require('./helper/AccountHelper');
const ScheduleHelper = require('./helper/ScheduleHelper');
const EmailHelper = require('./helper/EmailHelper');

//Setting Cross-Origin Headers
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Methods", "POST, PUT, GET, OPTIONS, DELETE");
    res.header("Access-Control-Allow-Headers", "Origin, X-RequestedWith, Content-Type, Accept");

    next();
});

app.use(express.static('public'));

//Setting express to use body parser
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

//Connect to the database
mongoose.connect(process.env.MONGODB_URI);

// Start jobs
ScheduleHelper.startScheduleCronJobs();

// Health Test //
app.get('/', async (req, res) => {
    return res.json({ success: true, message: "Api server is up and running..." });
});

// Feedback //
app.post('/api/feedback', async (req, res) => {
    const { csrv } = req.body;

    const result = await EmailHelper.sendFeedbackEmail(req.body);

    if (result === -1) {
        return { success: false, message: "Failed to send email inquiry. Please try again later or give us a call." }
    }

    return res.json({ success: true, message: "Contact us inquiry was sent." });
});

// Account //
app.post('/api/account/validate', async (req, res) => {
    const { csrv, username, password } = req.body;

    const result = await AccountHelper.validateLogin(username, password);

    return res.json(result);
});

app.get('/api/account/authenticate', async (req, res) => {
    const { token } = req.query;
    const isValidToken = await AccountHelper.validateToken(token);

    if (!isValidToken) {
        return res.json({ success: false, message: "Could not complete the request right now. Please try again later." });
    }

    return res.json({ success: true, message: "Token is valid." });
});

// Booking //
app.post('/api/booking/create', async (req, res) => {
    const { csrv, fullName, phone, email, vehicle, service, misc, bookingTime, bookingDate } = req.body;

    const result = await BookingHelper.createBooking(fullName, phone, email, vehicle, service, bookingDate, bookingTime, misc);

    return res.json(result);
});

app.put('/api/booking/update', async (req, res) => {
    const { csrv, id, fullName, phone, email, vehicle, service, misc, bookingTime, bookingDate, token, status } = req.body;

    const isValidToken = await AccountHelper.validateToken(token);

    if (!isValidToken) {
        return res.json({ success: false, message: "Could not complete the request right now. Please try again later." });
    }

    const result = await BookingHelper.updateBooking(id, fullName, phone, email, vehicle, service, bookingDate, bookingTime, misc, status);

    return res.json(result);
});

app.get('/api/booking/schedule/list', async (req, res) => {
    const { date } = req.query;

    const result = await BookingHelper.getAvailableBookingTimes(date);

    return res.json(result);
});

app.get('/api/booking/list', async (req, res) => {
    const { token, page } = req.query;
    const isValidToken = await AccountHelper.validateToken(token);

    if (!isValidToken) {
        return res.json({ success: false, message: "Could not complete the request right now. Please try again later." });
    }

    const result = await BookingHelper.getOngoingBookings(page);

    return res.json(result);
});

app.get('/api/booking/single/:id', async (req, res) => {
    const { token } = req.query;
    const { id } = req.params;
    const isValidToken = await AccountHelper.validateToken(token);

    if (!isValidToken) {
        return res.json({ success: false, message: "Could not complete the request right now. Please try again later." });
    }

    const result = await BookingHelper.getSingleBooking(id);

    return res.json(result);
});

app.get('/api/booking/search', async (req, res) => {
    const { token, value, filter, page, type } = req.query;
    const isValidToken = await AccountHelper.validateToken(token);

    if (!isValidToken) {
        return res.json({ success: false, message: "Could not complete the request right now. Please try again later." });
    }

    const result = await BookingHelper.searchForBookings(type, value, filter, page);

    return res.json(result);
});

app.get('/api/booking/archive/list', async (req, res) => {
    const { token, page } = req.query;
    const isValidToken = await AccountHelper.validateToken(token);

    if (!isValidToken) {
        return res.json({ success: false, message: "Could not complete the request right now. Please try again later." });
    }

    const result = await BookingHelper.getArchiveBookings(page);

    return res.json(result);
});

//Web Listener //
app.listen(port, () => {
    console.log(`SERVER: Running on PORT "${port}".`);
});