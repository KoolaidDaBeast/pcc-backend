const mongoose = require('mongoose');

module.exports = mongoose.model('Account', new mongoose.Schema({
    username: String,
    password: String,
    token: String
}));