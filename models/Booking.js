const mongoose = require('mongoose');

module.exports = mongoose.model('Booking', new mongoose.Schema({
    fullName: String,
    phone: String,
    email: String,
    vehicle: Object,
    bookingDate: String,
    bookingTime: String,
    service: String,
    misc: Object,
    status: String,
    reminded: Boolean
}, { timestamps: true }));