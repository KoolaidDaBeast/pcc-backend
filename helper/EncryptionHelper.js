const crypto = require('crypto');

module.exports = {

    hash: function (secert, value) {
        var hash = crypto.createHmac('sha256', secert).update(value).digest('hex');

        if (hash.length > 256) {
            hash = hash.substring(0, 255);
        }

        return hash;
    },
}