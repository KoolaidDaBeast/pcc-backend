const api = require("../libs/SMSApi");
const smsTemplate = require('../sms_template.json')

function getFriendlyDate(d) {
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let dateSuffix = "th";

    if (String(d.getDate()).endsWith("1")) dateSuffix = "st";
    if (String(d.getDate()).endsWith("2")) dateSuffix = "nd";
    if (String(d.getDate()).endsWith("3")) dateSuffix = "rd";

    return `${days[d.getDay()]}, ${d.getDate()}${dateSuffix} of ${months[d.getMonth()]}`
}

module.exports = {
    calculateISO: function (date, time) {
        const hourMultiplier = time.endsWith("PM") && !time.startsWith("12") ? 12 : time.endsWith("AM") && time.startsWith("12") ? -12 : 0;
        const croppedTimeStr = time.replace("AM", "").replace("PM", "");
        let hour = 0, minute = 0, year = 0, month = 0, day = 0;

        // Hours
        hour = parseInt(croppedTimeStr.split(":")[0]);

        // Minutes
        if (croppedTimeStr.includes(":")) {
            minute = parseInt(croppedTimeStr.split(":")[1]);
        }

        // Year, Month, Day
        year = parseInt(date.split("-")[0]);
        month = parseInt(date.split("-")[1]);
        day = parseInt(date.split("-")[2]);

        return { hour: hour + hourMultiplier, minute: minute, second: 0, year: year, month: month, day: day }
    },

    getTemplate: function (template, meta) {
        try {
            let data = smsTemplate[template];
            data = data.replaceAll("{FULL_NAME}", meta?.fullName)
                .replaceAll("{FIRST_NAME}", meta?.fullName.split(" ")[0])
                .replaceAll("{DATE}", `${meta?.bookingDate}`)
                .replaceAll("{TIME}", `${meta?.bookingTime}`)
                .replaceAll("{VEHICLE}", `${meta?.vehicle?.year} ${meta?.vehicle?.make} ${meta?.vehicle?.model}`)
                .replaceAll("{COMMENTS}", meta?.misc?.comments);

            const ISO = this.calculateISO(meta?.bookingDate, meta?.bookingTime);
            const bookingDate = new Date(ISO.year, ISO.month - 1, ISO.day);

            data = data.replaceAll("{FRIENDLY_DATE}", getFriendlyDate(bookingDate));

            return data;
        }
        catch (err) {
            return undefined;
        }
    },

    sendSMS: async function (template, phone, bookingData) {
        var smsApi = new api.SMSApi(process.env.SMS_USERNAME, process.env.SMS_API_KEY);

        var smsMessage = new api.SmsMessage();

        smsMessage.source = "sdk";
        smsMessage.to = `+${phone}`;
        smsMessage.body = this.getTemplate(template, bookingData);

        var smsCollection = new api.SmsMessageCollection();

        smsCollection.messages = [smsMessage];

        const result = (await smsApi.smsSendPost(smsCollection)).response.body;

        return result;
    }
}