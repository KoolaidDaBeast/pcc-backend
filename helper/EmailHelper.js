const { EmailClient } = require("@azure/communication-email");
const fs = require('node:fs');

// Connect to email provider
const connectionString = process.env['AZURE_COMMS_URI'];
const emailClient = new EmailClient(connectionString);

function getFriendlyDate(d) {
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const months = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    let dateSuffix = "th";

    if (String(d.getDate()).endsWith("1")) dateSuffix = "st";
    if (String(d.getDate()).endsWith("2")) dateSuffix = "nd";
    if (String(d.getDate()).endsWith("3")) dateSuffix = "rd";

    return `${days[d.getDay()]}, ${d.getDate()}${dateSuffix} of ${months[d.getMonth()]}`
}

module.exports = {
    calculateISO: function (date, time) {
        const hourMultiplier = time.endsWith("PM") && !time.startsWith("12") ? 12 : time.endsWith("AM") && time.startsWith("12") ? -12 : 0;
        const croppedTimeStr = time.replace("AM", "").replace("PM", "");
        let hour = 0, minute = 0, year = 0, month = 0, day = 0;

        // Hours
        hour = parseInt(croppedTimeStr.split(":")[0]);

        // Minutes
        if (croppedTimeStr.includes(":")) {
            minute = parseInt(croppedTimeStr.split(":")[1]);
        }

        // Year, Month, Day
        year = parseInt(date.split("-")[0]);
        month = parseInt(date.split("-")[1]);
        day = parseInt(date.split("-")[2]);

        return { hour: hour + hourMultiplier, minute: minute, second: 0, year: year, month: month, day: day }
    },

    getFeedbackTemplate: function (meta) {
        try {
            let data = fs.readFileSync('./template_contactus.html', 'utf8');
            data = data.replaceAll("{NAME}", meta?.fullName)
                .replaceAll("{PHONE}", meta?.phone)
                .replaceAll("{EMAIL}", meta?.email)
                .replaceAll("{CAR}", meta?.car)
                .replaceAll("{MESSAGE}", meta?.message);

            return data;
        }
        catch (err) {
            return undefined;
        }
    },

    getTemplate: function (template, meta) {
        try {
            let data = fs.readFileSync('./' + template + '.html', 'utf8');
            data = data.replaceAll("{FULL_NAME}", meta?.fullName)
                .replaceAll("{FIRST_NAME}", meta?.fullName.split(" ")[0])
                .replaceAll("{DATE}", `${meta?.bookingDate}`)
                .replaceAll("{TIME}", `${meta?.bookingTime}`)
                .replaceAll("{VEHICLE}", `${meta?.vehicle?.year} ${meta?.vehicle?.make} ${meta?.vehicle?.model}`)
                .replaceAll("{COMMENTS}", meta?.misc?.comments);

            const ISO = this.calculateISO(meta?.bookingDate, meta?.bookingTime);
            const bookingDate = new Date(ISO.year, ISO.month - 1, ISO.day);

            data = data.replaceAll("{FRIENDLY_DATE}", getFriendlyDate(bookingDate));

            return data;
        }
        catch (err) {
            return undefined;
        }
    },

    sendEmail: async function (template, toAddress, subject, meta) {
        try {
            const emailMessage = {
                senderAddress: process.env.EMAIL_DOMAIN,
                content: {
                    subject: subject,
                    html: this.getTemplate(template, meta)
                },
                recipients: {
                    to: [{ address: toAddress }],
                },
            };

            const poller = await emailClient.beginSend(emailMessage);
            // const result = await poller.pollUntilDone();
        }
        catch {
            console.log("LOG: Failed to send the email to " + toAddress);
        }
    },

    sendFeedbackEmail: async function (meta) {
        try {
            const toAddress = process.env.CONTACT_US_EMAIL;
            const emailMessage = {
                senderAddress: process.env.EMAIL_DOMAIN,
                content: {
                    subject: "Contact Us - Inquiry",
                    html: this.getFeedbackTemplate(meta)
                },
                recipients: {
                    to: [{ address: toAddress }],
                },
            };

            const poller = await emailClient.beginSend(emailMessage);
            // const result = await poller.pollUntilDone();
        }
        catch {
            return -1;
        }
    }
}