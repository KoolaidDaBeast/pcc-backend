const Booking = require('../models/Booking');
const EmailHelper = require('./EmailHelper');
const SMSHelper = require('./SMSHelper');
const TimeRestrictions = require('../times.json');
const mongoose = require('mongoose');

function isNumeric(value) {
    return /^-?\d+$/.test(value);
}

function validateBookingDetails(booking) {
    try {
        if ("bookingDate" in booking) {
            if (booking.bookingDate.split("-").length !== 3) {
                return false;
            }

            let yr = isNumeric(booking.bookingDate.split("-")[0]);
            let month = isNumeric(booking.bookingDate.split("-")[1]);
            let day = isNumeric(booking.bookingDate.split("-")[2]);

            if ((yr && month && day) == false) {
                return false;
            }
        }

        if ("phone" in booking) {
            const lengthCheck = String(booking.phone).length === 10;
            const numberCheck = isNumeric(booking.phone);

            if ((lengthCheck && numberCheck) == false) {
                return false;
            }
        }

        if ("email" in booking) {
            const email = booking.email;
            const atCheck = email.split("@").length === 2;
            const dotCheck = email.split("@")[1].split(".").length === 2;

            if (atCheck && dotCheck == false) {
                return false;
            }
        }

        return true;
    }
    catch {
        return false;
    }
}

module.exports = {
    isDatabaseConnected: async function () {
        return mongoose.connection.readyState === 1;
    },

    generateDefaultTimes: function (day, filteredTimes) {
        let values = [];
        const start = parseInt(TimeRestrictions[day].split("-")[0]);
        const end = parseInt(TimeRestrictions[day].split("-")[1]);

        for (let idx = start; idx <= end; idx++) {
            let value = "";

            if (idx == 12) {
                value = `${idx}PM`;
            }
            else if (idx > 12) {
                value = `${idx - 12}PM`;
            }
            else {
                value = `${idx}AM`;
            }

            if (filteredTimes.includes(value)) continue;

            values.push(value);
        }

        return values;
    },

    isValidBookingId: async function (id) {
        const connected = this.isDatabaseConnected();

        if (!connected) {
            return {
                success: false,
                message: "Database connections are not active..."
            }
        }

        try {
            var promise = Booking.findOne({ _id: id });
            const promiseValue = await promise;
            return promiseValue == null || promiseValue == undefined ? false : true;
        }
        catch {
            return false;
        }
    },

    getSingleBooking: async function (id) {
        const connected = this.isDatabaseConnected();

        if (!connected) {
            return {
                success: false,
                message: "Database connections are not active..."
            }
        }

        try {
            var promise = Booking.findOne({ _id: id });
            const promiseValue = await promise;
            return { success: true, value: promiseValue };
        }
        catch {
            return { success: false, value: {} }
        }
    },

    getScheduledBookings: async function () {
        const connected = this.isDatabaseConnected();

        if (!connected) {
            return {
                success: false,
                message: "Database connections are not active..."
            }
        }

        const promiseValue = await Booking.find({ status: "approved", reminded: false });

        return promiseValue;
    },

    getOngoingBookings: async function (page) {
        const connected = this.isDatabaseConnected();
        const perPage = 100;

        if (!connected) {
            return {
                success: false,
                message: "Database connections are not active..."
            }
        }

        const promiseValue = await Booking.find({ $and: [{ status: { "$ne": "done" } }, { status: { "$ne": "cancelled" } }] }).sort({ createdAt: -1 }).limit(perPage).skip(perPage * page);

        return promiseValue;
    },

    getArchiveBookings: async function (page) {
        const connected = this.isDatabaseConnected();
        const perPage = 100;

        if (!connected) {
            return {
                success: false,
                message: "Database connections are not active..."
            }
        }

        const promiseValue = await Booking.find({ $or: [{ status: "done" }, { status: "cancelled" }] }).sort({ updatedAt: -1 }).limit(perPage).skip(perPage * page);

        return promiseValue;
    },

    searchForBookings: async function (type, value, filter, page = 0) {
        const connected = this.isDatabaseConnected();
        const perPage = 100;

        if (!connected) {
            return {
                success: false,
                message: "Database connections are not active..."
            }
        }

        filter = filter === "name" ? "fullName" : filter;

        let promiseValue = [];
        const bookingType = type === "archive" ? { $or: [{ status: "done" }, { status: "cancelled" }] } : { $and: [{ status: { "$ne": "done" } }, { status: { "$ne": "cancelled" } }] };

        if (filter === "vehicle") {
            promiseValue = await Booking.find({
                $and: [bookingType, { $or: [{ 'vehicle.make': { "$regex": value, "$options": "i" } }, { 'vehicle.model': { "$regex": value, "$options": "i" } }, { 'vehicle.year': { "$regex": value, "$options": "i" } }] }]
            }).sort({ createdAt: -1 }).limit(perPage).skip(perPage * page);
        }
        else if (filter === "rego") {
            promiseValue = await Booking.find({
                $and: [bookingType, { 'vehicle.rego': { "$regex": value, "$options": "i" } }]
            }).sort({ createdAt: -1 }).limit(perPage).skip(perPage * page);
        }
        else {
            promiseValue = await Booking.find({
                $and: [bookingType, { [filter]: { "$regex": value, "$options": "i" } }]
            }).sort({ createdAt: -1 }).limit(perPage).skip(perPage * page);
        }

        return { success: true, results: promiseValue };
    },

    isBookingSlotFree: async function (date, time) {
        const connected = this.isDatabaseConnected();

        if (!connected) {
            return {
                success: false,
                message: "Database connections are not active..."
            }
        }

        var promise = Booking.findOne({ bookingDate: date, bookingTime: time });
        const promiseValue = await promise;

        return promiseValue == undefined || promiseValue == null ? true : false;
    },

    getAvailableBookingTimes: async function (date) {
        const connected = this.isDatabaseConnected();

        if (!connected) {
            return {
                success: false,
                message: "Database connections are not active..."
            }
        }

        const promiseValue = await Booking.find({ bookingDate: date });
        let values = this.generateDefaultTimes("*", promiseValue.map(t => t.bookingTime));

        return { success: true, availableSlots: values };
    },

    createBooking: async function (fullName, phoneNumber, emailAddress, vehicleData, serviceType, bookingDate, bookingTime, miscData) {
        const connected = this.isDatabaseConnected();

        if (!connected) {
            return {
                success: false,
                message: "Database connections are not active..."
            }
        }

        if (bookingDate == undefined || bookingTime == undefined) {
            return {
                success: false,
                message: "Please provide booking time and date information..."
            }
        }

        // Check booking time
        if (!(await this.isBookingSlotFree(bookingDate, bookingTime))) {
            return {
                success: false,
                message: "That booking time slot is already taken..."
            }
        }

        const bookingObj = {
            fullName: fullName,
            phone: phoneNumber,
            email: emailAddress,
            vehicle: vehicleData,
            bookingDate: bookingDate,
            bookingTime: bookingTime,
            service: serviceType,
            misc: miscData,
            status: "awaiting_approval",
            reminded: false
        };

        // Validate the inputs
        const isValid = validateBookingDetails(bookingObj);

        if (!isValid) {
            return { success: false, message: "Bad data please check inputs filled correctly." };
        }

        var promise = Booking.create(bookingObj);

        const promiseValue = await promise;

        if (promiseValue == null || promiseValue == undefined) {
            return { success: false, message: "Failed to store booking information in the database." }
        }

        // Send pre-confirm email
        const emailMetaData = promiseValue;
        await EmailHelper.sendEmail("template_waiting", emailAddress, "PCC - Booking Received", emailMetaData);
        // await SMSHelper.sendSMS("template_waiting", phoneNumber, emailMetaData);

        return { success: true, id: promiseValue._id, message: "Booking was created. Please wait for apporval you will recieve an email shortly." };
    },

    updateBooking: async function (id, fullName, phoneNumber, emailAddress, vehicleData, serviceType, bookingDate, bookingTime, miscData, status) {
        const ScheduleHelper = require('./ScheduleHelper');
        const filter = { _id: id };
        let updatedData = {};

        if (fullName != undefined) updatedData.fullName = fullName;
        if (phoneNumber != undefined) updatedData.phone = phoneNumber;
        if (emailAddress != undefined) updatedData.email = emailAddress;
        if (vehicleData != undefined) updatedData.vehicle = vehicleData;
        if (serviceType != undefined) updatedData.service = serviceType;
        if (bookingDate != undefined) updatedData.bookingDate = bookingDate;
        if (bookingTime != undefined) updatedData.bookingTime = bookingTime;
        if (miscData != undefined) updatedData.misc = miscData;
        if (status != undefined) updatedData.status = status;

        // Validate the inputs
        const isValid = validateBookingDetails(updatedData);

        if (!isValid) {
            return { success: false, message: "Bad data please check inputs filled correctly." };
        }

        const bookingIdExists = await this.isValidBookingId(id);

        if (!bookingIdExists) {
            return { success: false, message: "The booking id to update is invalid." }
        }

        try {
            var promiseValue = await Booking.findOneAndUpdate(filter, updatedData, { new: true });

            // Turn on reminders
            ScheduleHelper.startSingleScheduleCronJob(promiseValue);

            //TODO: Send confirmation email and SMS
            if (status === "approved") {
                // await SMSHelper.sendSMS("template_confirmed", promiseValue.phone, promiseValue);
                await EmailHelper.sendEmail("template_confirmed", promiseValue?.email, "PCC - Booking Confirmed", promiseValue);
            }
            if (status === "cancelled") {
                // await SMSHelper.sendSMS("template_cancelled", promiseValue.phone, promiseValue);
                await EmailHelper.sendEmail("template_cancelled", promiseValue?.email, "PCC - Booking Cancelled", promiseValue);
            }
            if (status === "done") {
                // await SMSHelper.sendSMS("template_done", promiseValue.phone, promiseValue);
            }

            return { success: true, data: promiseValue };
        }
        catch (e) {
            return { success: false, data: {} };
        }
    },

    setBookingStatus: async function (id, newStatus) {
        const filter = { _id: "id" };

        try {
            var promiseValue = await Booking.findOneAndUpdate(filter, { status: newStatus }, { new: true });

            return { value: promiseValue };
        }
        catch {
            return { value: undefined };
        }
    },

    setBookingReminder: async function (id, reminded) {
        const filter = { _id: id };

        try {
            var promiseValue = await Booking.findOneAndUpdate(filter, { reminded: reminded }, { new: true });

            return { value: promiseValue };
        }
        catch {
            return { value: undefined };
        }
    },
}