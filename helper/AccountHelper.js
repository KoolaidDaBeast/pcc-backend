const Account = require("../models/Account");
const EncryptionHelper = require("./EncryptionHelper");

module.exports = {
    generateAdminAccount: async function (password) {
        var promise = Account.create({
            username: "pcc_admin",
            password: EncryptionHelper.hash(process.env.SALT_KEY, password)
        });

        const promiseValue = await promise;

        return promiseValue;
    },

    generateAdminToken: async function () {
        const filter = { username: "pcc_admin" };

        const updatedToken = EncryptionHelper.hash(new Date().toDateString(), String(Math.random() * 99999));
        var promiseValue = await Account.findOneAndUpdate(filter, { token: updatedToken }, { new: true });

        return { value: promiseValue.token };
    },

    validateLogin: async function (username, password) {
        var promise = Account.findOne({ username: username });
        const promiseValue = await promise;

        // Check if username is correct
        if (promiseValue == null || promiseValue == undefined) {
            return { success: false, message: "Invalid login creds..." }
        }

        // Check password is correct
        if (promiseValue.password !== EncryptionHelper.hash(process.env.SALT_KEY, password)) {
            return { success: false, message: "Invalid login creds..." }
        }

        // Generate token
        const updatedToken = await this.generateAdminToken();

        return { success: true, message: "Login creds were successfull...", token: updatedToken.value }
    },

    validateToken: async function (token) {
        var promise = Account.findOne({ token: token });
        const promiseValue = await promise;

        return promiseValue == undefined || promiseValue == null ? false : true;
    }
}