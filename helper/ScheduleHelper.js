const BookingHelper = require("./BookingHelper");
const EmailHelper = require("./EmailHelper");
const SMSHelper = require("./SMSHelper");
const CronJob = require('croner');

module.exports = {
    calculateISO: function (date, time) {
        const hourMultiplier = time.endsWith("PM") && !time.startsWith("12") ? 12 : time.endsWith("AM") && time.startsWith("12") ? -12 : 0;
        const croppedTimeStr = time.replace("AM", "").replace("PM", "");
        let hour = 0, minute = 0, year = 0, month = 0, day = 0;

        // Hours
        hour = parseInt(croppedTimeStr.split(":")[0]);

        // Minutes
        if (croppedTimeStr.includes(":")) {
            minute = parseInt(croppedTimeStr.split(":")[1]);
        }

        // Year, Month, Day
        year = parseInt(date.split("-")[0]);
        month = parseInt(date.split("-")[1]);
        day = parseInt(date.split("-")[2]);

        return { hour: hour + hourMultiplier, minute: minute, second: 0, year: year, month: month, day: day }
    },

    calcuateCronExprs: function (date, time) {
        const ISO = this.calculateISO(date, time);
        let DateISO = new Date(ISO.year, ISO.month - 1, ISO.day + 1);
        DateISO.setDate(DateISO.getDate() - 1);
        // console.log("ISO:", ISO)

        const updatedISOHour = String(ISO.hour - 1 < 0 ? 0 : ISO.hour - 1);
        const updatedISOMinute = String(ISO.minute).length === 1 ? "0" + String(ISO.minute) : String(ISO.minute);

        return {
            oneDayBefore: DateISO.toISOString().substring(0, DateISO.toISOString().length - 2),
            oneHourBefore: `${date}T${updatedISOHour.length === 1 ? "0" : ""}${updatedISOHour}:${updatedISOMinute}:00`
        }
    },

    startScheduleCronJobs: async function () {
        const bookings = await BookingHelper.getScheduledBookings()

        if (bookings.length == 0) {
            console.log(`CRON: No scheduled jobs available...`);
            console.error()
            return;
        }

        console.log('----------------------------');

        bookings.forEach(booking => {
            this.startSingleScheduleCronJob(booking)
        });
    },

    startSingleScheduleCronJob: async function (booking) {
        if (booking == undefined || booking == null) {
            console.log("ERROR: Invalid booking data suppilied to CRON.")
            return;
        }

        // Stop previous jobs
        CronJob.scheduledJobs.forEach(job => {
            if (job.name.startsWith(booking?._id)) {
                job.stop();
                console.log(`CRON: Stopping previous booking "${booking?._id}" reminder.`);
            }
        });

        // Double check status
        if (booking?.status !== "approved" || booking?.reminded) {
            return;
        }

        const { bookingDate, bookingTime } = booking;
        const cronID = booking._id;
        const cronExpr = this.calcuateCronExprs(bookingDate, bookingTime);

        const toScheduleDate = new Date(`${cronExpr.oneHourBefore}`);
        const nowDate = new Date();

        if (nowDate > toScheduleDate) {
            await BookingHelper.setBookingReminder(cronID, true);
            console.log(`CRON: Booking "${cronID}" time has already passed skipping...`);
            return;
        }

        CronJob(cronExpr.oneHourBefore, { name: `${cronID}_${String(new Date().getTime())}`, timezone: "Australia/Melbourne" }, async () => {
            const cacheBooking = booking;

            // Send reminder email
            await EmailHelper.sendEmail("template_reminder", cacheBooking.email, "PCC - Booking Reminder", cacheBooking);

            //TODO: Send reminder SMS
            // await SMSHelper.sendSMS("template_reminder", cacheBooking.phone, cacheBooking);

            // Update database
            await BookingHelper.setBookingReminder(cacheBooking._id, true);
        });

        CronJob(cronExpr.oneDayBefore, { name: `${cronID}_${String(new Date().getTime())}`, timezone: "Australia/Melbourne" }, async () => {
            const cacheBooking = booking;

            // Send reminder email
            await EmailHelper.sendEmail("template_reminder", cacheBooking.email, "PCC - Booking Reminder", cacheBooking);

            //TODO: Send reminder SMS
            // await SMSHelper.sendSMS("template_reminder", cacheBooking.phone, cacheBooking);

            // Update database
            await BookingHelper.setBookingReminder(cacheBooking._id, true);
        });

        console.log(`CRON: Scheduled booking with id "${booking._id}" @ ${cronExpr.oneHourBefore}...`);
        console.log(`CRON: Scheduled booking with id "${booking._id}" @ ${cronExpr.oneDayBefore}...`);
    }
}
